public class Planetas {

	protected String equip;
	protected double misils;
    protected int tipo;
	protected double vida;
	protected int victorias;
	protected	int ronda;
	
	public Planetas (String nom ) {
		equip = nom;
		misils=50;
		vida = 200;
		tipo=0;
		ronda = 1;
	}
	public String getEquip() {
		return equip;
	}
	public double getMisils() {
		return misils;
	}
	public double getVida() {
		return vida;
	}
    public int getTipo() {
      	 return tipo;
       }
    public int getVictoria() {
     	 return tipo;
      }
	public int getRonda() {
		return ronda;
	}



	public void setEquip(String e) {
		this.equip=e;
	}

	public void setMisils(double d) {
		this.misils= d;
	}
	public void setVida(double d) {
		this.vida = d ;
	}
    public void setTipo(int v) {
      	 tipo = v ;
       }
    public void setVictoria(int v) {
     	 tipo = v ;
      }
	public void setRonda(int ronda) {
		this.ronda = ronda;
	}
}
