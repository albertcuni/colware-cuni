import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class ContenidoMarco extends JPanel implements ActionListener{
	

	static ArrayList<Planetas> equips = new ArrayList<Planetas>(); // Equips que crearem

	//Botons
	private JButton jugar;
	private JButton informacio;
	private JButton sortir;
	private JButton crearEquip;
	private JButton equipNou;
	private JButton tornar;
	private JButton atac;
	private JButton defensar;
	private JButton siguiente;
	private JButton ranking;
	private JButton guarda;
	private JButton cargar_Partida;
	private JButton partidaG;

	private JTextArea  numeroEquipsT;
	private JTextArea  nomEquip;
	private JTextArea descripcioRonda;
	private JTextArea descripcioTipus;
	private JTextArea  xml;
	
	private JComboBox comboNombreEquipos;
	private JComboBox comboMisils;
	private JComboBox comboTipus;
	private JComboBox partidas;

	private JLabel informacio1;
	private JLabel informacio2;  
	private JLabel informacio3;
	private JLabel textCrearEquip;
	private JLabel textEquip;
	private JLabel ronda;
	private JLabel turno;
	private JLabel atacar;
	private JLabel inforonda;
	private JLabel textGuanyador;
	
	private int con = 0;
	private int numRonda = 1;
	private int numTurno = 1;
	private int turnoEquipoArray = 0;
	private int tipoPlaneta = 1;
	public static int persona = 0;
	public static int misils = 50;
	public static int turnos = 0;
	public static int  id_partidac=0;
	public static int carregar_Partida;
	
	private String numEquips = "";
	private String nombreTurnoEquipo = "";
	public static String guanyador;
	public static String equipAtacat;
	public static String equipDefensat;
	public static String equipEnPartida;
	public static String textAfegir = " ";
	public static String vidaEquips= " ";
	
	public static final String xmlFilePath ="E:\\Dam2 2020\\coldward-cuni\\ranking.xml";
	public static String tipo;
	public static String leerXml = "Lectura XML";

	
	private static final String DB = "DatabaseName=DAM2_CUNILLERABENETALBERT_COLDWAR;";
	private static final String USER = "user=DAM2_39528338D;";
	private static final String PWD = "Password=Albertcuni11*;";
	private static final String URL = "jdbc:sqlserver://oracle.ilerna.com;" + DB + USER + PWD;
	public static  Connection connection;

	public ContenidoMarco(){
		super();

		connection = makeConnection();
		ContingutsDiferents();
		setBackground(Color.gray);
		
	}

	public void ContingutsDiferents() {

		if(Main.contenido==0) {//menu
			//Aqui el menu
			

			informacio1 = new JLabel("COLDWAR CUNI");
			informacio1.setFont(new Font("Montserrat", Font.BOLD, 40));
			informacio1.setBounds(100, -50, 800, 200);   // colocamos posicion y tamanio al texto (x, y, ancho, alto)
			this.add(informacio1);


			jugar = new JButton("JUGAR");
			jugar.setFont(new Font("Montserrat", Font.BOLD, 15));
			jugar.setBounds(120, 140, 200, 30);
			this.add(jugar);
			jugar.addActionListener(this);

			informacio = new JButton("INFORMACIO");
			informacio.setFont(new Font("Montserrat", Font.BOLD, 15));
			informacio.setBounds(120, 180, 200, 30);
			this.add(informacio);
			informacio.addActionListener(this);
			
			sortir = new JButton("SURTIR");
			sortir.setFont(new Font("Montserrat", Font.BOLD, 15));
			sortir.setBounds(120, 220, 200, 30);
			this.add(sortir);
			sortir.addActionListener(this);
			
			cargar_Partida = new JButton("CARREGAR PARTIDA");
			cargar_Partida.setFont(new Font("Montserrat", Font.BOLD, 15));
			cargar_Partida.setBounds(120, 260, 200, 30);
			this.add(cargar_Partida);
			cargar_Partida.addActionListener(this);
			
			ranking = new JButton("Ranking");
			ranking.setFont(new Font("Montserrat", Font.BOLD, 15));
			ranking.setBounds(120, 300, 200, 30);
			this.add(ranking);
			ranking.addActionListener(this);

			setLayout(null);


		}else if(Main.contenido == 1) { //Equips que juguen

			//Mostrar text
			textCrearEquip = new JLabel("Quans equips jugareu?");
			textCrearEquip.setFont(new Font("Montserrat", Font.BOLD, 20));
			textCrearEquip.setBounds(130, -80, 900, 200); 
			this.add(textCrearEquip);

			//Introduir Text
			numeroEquipsT = new JTextArea();
			numeroEquipsT.setBounds(150, 60, 50, 20); 
			this.add(numeroEquipsT);

			//Boto crear equips
			crearEquip = new JButton("Crear Equips");
			crearEquip.setFont(new Font("Montserrat", Font.BOLD, 20));
			crearEquip.setBounds(120, 100, 200, 30);
			this.add(crearEquip);
			crearEquip.addActionListener(this);

			//Tornar al menu
			tornar = new JButton("Torna Menu");
			tornar.setFont(new Font("Montserrat", Font.BOLD, 20));
			tornar.setBounds(120, 140, 200, 30);
			this.add(tornar);
			tornar.addActionListener(this);

		}else if (Main.contenido == 2) {//Crear equips

			numEquips = numeroEquipsT.getText();        

			int indiceEquipo = con+1;
			textEquip = new JLabel("Equip "+indiceEquipo);
			textEquip.setFont(new Font("Montserrat", Font.BOLD, 20));
 			textEquip.setBounds(130, -80, 900, 200); 
			this.add(textEquip);

			//Introduir Text
			nomEquip = new JTextArea();
			nomEquip.setBounds(150, 60, 90, 20); 
			this.add(nomEquip);

			descripcioTipus = new JTextArea();
			comboTipus = new JComboBox<String>();
			comboTipus.setFont( new Font ("Arial",Font.BOLD,15));
			comboTipus.addItem("Normal");
			comboTipus.addItem("Aquatic");
			comboTipus.addItem("Selva");
			comboTipus.addItem("Foc");
			comboTipus.addItem("Gas");
			comboTipus.setBounds(130, 150, 200, 20);
			descripcioTipus.setBounds(130, 200, 300, 200);
			descripcioTipus.setText("No te cap counter");
			descripcioTipus.append("\n Es normal.");
			descripcioTipus.setEditable(false);
			comboTipus.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {

					if ( comboTipus.getSelectedItem().toString().equals("Normal") ) {

						descripcioTipus.setText("No te cap counter");
						descripcioTipus.append("/n Tot normal.");
						
						tipoPlaneta = 1;

					}else if ( comboTipus.getSelectedItem().toString().equals("Aquatic") ) {

						descripcioTipus.setText("Te el doble de vida.");
						descripcioTipus.setText("I el doble de misils.");
						

						tipoPlaneta = 2;

					}else if ( comboTipus.getSelectedItem().toString().equals("Selva") ) {

						descripcioTipus.setText("Al atacar te el doble de danyo");

						tipoPlaneta = 3 ;


					}else if ( comboTipus.getSelectedItem().toString().equals("Foc") ) {

						descripcioTipus.setText("Ataca amb la meitat de misils ");
						descripcioTipus.append("\n Pero te el doble de vida.");

						tipoPlaneta = 4;

						
					}else if ( comboTipus.getSelectedItem().toString().equals("Gas") ) {

						descripcioTipus.setText("Vida inicial de 400.");	
						tipoPlaneta = 5;

					}
				}
			});

			this.add(comboTipus);			
			this.add(descripcioTipus);
			//Boto crear equips
			equipNou = new JButton("Crea equip "+indiceEquipo);
			equipNou.setFont(new Font("Montserrat", Font.BOLD, 20));
			equipNou.setBounds(120, 100, 200, 30);
			this.add(equipNou);
			equipNou.addActionListener(this);	


		}else if (Main.contenido == 3) {//Text informatiu


			informacio1 = new JLabel("Versi actual: Coldwar.V01.06.1577.beta");
			informacio1.setBounds(50, -50, 800, 200);   // colocamos posicion y tamanio al texto (x, y, ancho, alto)
			informacio1.setFont(new Font("Montserrat", Font.BOLD, 15));
			this.add(informacio1);

			informacio2 = new JLabel("Aquest programa est creat i desenvolupat "+ "\n"+ "per  Albert Cunillera ");
			informacio2.setFont(new Font("Montserrat", Font.BOLD, 15));
			informacio2.setBounds(30, -30, 800, 200);   // colocamos posicion y tamanio al texto (x, y, ancho, alto)
			this.add(informacio2);


			//Boto per tornar al menu

			tornar = new JButton("TORNA AL MENU");
			tornar.setFont(new Font("Montserrat", Font.BOLD, 15));
			tornar.setBounds(150, 90, 200, 30);
			this.add(tornar);
			tornar.addActionListener(this);


		}else if (Main.contenido == 4) {//Partida
			int numEquiois;
			//Array per guarda nom dels equips
			if(	numeroEquipsT != null) {
			 numEquiois = (Integer.parseInt(numeroEquipsT.getText())) -1;
			}
			String[] nomEquips = new String[equips.size()];
			
				numEquiois = equips.size();

			//Text
			ronda = new JLabel("Ronda "+equips.get(0).getRonda());
			ronda.setFont(new Font("Montserrat", Font.BOLD, 20));
			ronda.setBounds(130, -80, 900, 200); 
			this.add(ronda);
			System.out.println("Numero de ronda " + equips.get(0).getRonda());
			equipEnPartida =equips.get(numTurno-1).getEquip();
			//Text
			turno = new JLabel("Torn numero "+numTurno + " Equipo "+ equipEnPartida);
			turno.setFont(new Font("Montserrat", Font.BOLD, 20));
			turno.setBounds(130, -40, 900, 200); 
			this.add(turno);
			System.out.println("Torn numero "+numTurno+" Equip "+equipEnPartida);

			//Text
			atacar = new JLabel("Aqui vols atacar?");
			atacar.setFont(new Font("Montserrat", Font.BOLD, 15));
			atacar.setBounds(130, -20, 900, 200); 
			this.add(atacar);
			//Combo Nom equips
			int con = 0;
			int ronda = equips.get(0).getRonda() + 1;
			for(int contadorEquips=0;contadorEquips<equips.size();contadorEquips++ ) {
				if(!equips.get(numTurno-1).getEquip().equals(equips.get(contadorEquips).getEquip()))  { //&& equips.get(numTurno-1).getVida() > 0 && ronda > numRonda
					nomEquips[con]=equips.get(contadorEquips).getEquip();
					con++;

				}
			}

			//Combo box nom equips
			comboNombreEquipos = new JComboBox<String>(nomEquips);
			comboNombreEquipos.setBounds(130, 150, 200, 20);
			this.add(comboNombreEquipos);

			//Combo misils 
			comboMisils = new JComboBox<String>();
			comboMisils.setBounds(130, 180, 200, 20);

			for(int misil =1 ;misil<=misils;misil++) {

				comboMisils.addItem(String.valueOf(misil));
			}
			this.add(comboMisils);

			atac = new JButton("Atacar");
			atac.setFont(new Font("Montserrat", Font.BOLD, 15));
			atac.setBounds(120, 220, 100, 30);
			this.add(atac);
			atac.addActionListener(this);

			defensar = new JButton("Defensar");
			defensar.setFont(new Font("Montserrat", Font.BOLD, 15));
			defensar.setBounds(250, 220, 100, 30);
			this.add(defensar);
			defensar.addActionListener(this);

		}else if(Main.contenido == 5){
			this.removeAll();
			this.repaint();
			 con = 0;
			 numRonda = 1;
			 numTurno = 1;
			 turnoEquipoArray = 0;
			 tipoPlaneta = 1;
			 persona = 0;
			 misils = 50;
			 turnos = 0;
			
			textGuanyador = new JLabel("El equip guanyador es "+equips.get(0).getEquip());
			textGuanyador.setBounds(40, 90, 800, 150);   // colocamos posicion y tamanio al texto (x, y, ancho, alto)
			textGuanyador.setFont(new Font("Montserrat", Font.BOLD, 15));
			this.add(textGuanyador);
			leerXML(equips);
			textAfegir = " ";
			vidaEquips = " ";
			//Tornar al menu
			tornar = new JButton("Torna a jugar");
			tornar.setFont(new Font("Montserrat", Font.BOLD, 20));
			tornar.setBounds(60, 200, 200, 30);
			this.add(tornar);
			tornar.addActionListener(this);
			
			ranking = new JButton("Ranking");
			ranking.setFont(new Font("Montserrat", Font.BOLD, 15));
			ranking.setBounds(60, 400, 150, 30);
			this.add(ranking);
			ranking.addActionListener(this);
			
			
		}else if(Main.contenido == 6){
			this.removeAll();
			this.repaint();


			inforonda = new JLabel("Descripc�o");
			inforonda.setBounds(40, 90, 800, 150);   // colocamos posicion y tamanio al texto (x, y, ancho, alto)
			inforonda.setFont(new Font("Montserrat", Font.BOLD, 15));
			this.add(inforonda);

			descripcioRonda = new JTextArea();
			descripcioRonda.setBounds(150, 90, 300, 150);
			descripcioRonda.setText(textAfegir);
			descripcioRonda.append(vidaEquips);
			descripcioRonda.setEditable(false);
			this.add(descripcioRonda);


			siguiente = new JButton("siguiente ");
			siguiente.setFont(new Font("Montserrat", Font.BOLD, 20));
			siguiente.setBounds(150, 300, 150, 30);
			this.add(siguiente);
			siguiente.addActionListener(this);
			
			guarda = new JButton("Guarda partida");
			guarda.setFont(new Font("Montserrat", Font.BOLD, 20));
			guarda.setBounds(150, 350, 150, 30);
			this.add(guarda);
			guarda.addActionListener(this);

			for (int x=0; x<equips.size();x++) {
				if(equips.get(x).getVida()<= 0) {
					equips.remove(equips.get(x));
				}
				if(equips.size()== 1) {
					Main.contenido = 5;
					ContingutsDiferents();
				}
			}

		}else if(Main.contenido == 7){
			this.removeAll();
			this.repaint();
			Ranking();
			xml = new JTextArea();
			xml.setBounds(30, 30, 425, 375);
			xml.setText(leerXml);
			xml.setEditable(false);
			this.add(xml);
			
			tornar = new JButton("Torna Menu");
			tornar.setFont(new Font("Montserrat", Font.BOLD, 20));
			tornar.setBounds(150, 410, 200, 40);
			this.add(tornar);
			tornar.addActionListener(this);		
		}else if(Main.contenido == 8){
			this.removeAll();
			this.repaint();
			
			
			informacio3 = new JLabel("Selecciona la partida que vols jugar");
			informacio3.setFont(new Font("Montserrat", Font.BOLD, 15));
			informacio3.setBounds(120, 30, 800, 200);   // colocamos posicion y tamanio al texto (x, y, ancho, alto)
			this.add(informacio3);
			
			//Combo box nom equips
			partidas = new JComboBox<String>();
			partidas.setBounds(120, 150, 200, 20);
			this.add(partidas);
			
			partidaG = new JButton("CARREGAR PARTIDA");
			partidaG.setFont(new Font("Montserrat", Font.BOLD, 15));
			partidaG.setBounds(120, 180, 200, 30);
			this.add(partidaG);
			partidaG.addActionListener(this);
			
			
			for(int x =1 ;x<=id_partidac;x++) {
				partidas.addItem(String.valueOf(x));
			}
			
			tornar = new JButton("TORNA AL MENU");
			tornar.setFont(new Font("Montserrat", Font.BOLD, 15));
			tornar.setBounds(120, 225, 200, 30);
			this.add(tornar);
			tornar.addActionListener(this);	
			
		}
		
}
	void Ranking() {
		try {
			leerXml = "Lectura XML";

			File archivo = new File("E:\\Dam2 2020\\coldward-cuni\\ranking.xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document document = documentBuilder.parse(archivo);
			document.getDocumentElement().normalize();
			System.out.println("Elemento raiz:" + document.getDocumentElement().getNodeName());
			NodeList listaEmpleados = document.getElementsByTagName("equipos");
			for (int temp = 0; temp < listaEmpleados.getLength(); temp++) {
				Planetas planetas = new Planetas("");

				Node nodo = listaEmpleados.item(temp);
				//System.out.println("Elemento:" + nodo.getNodeName());
				if (nodo.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) nodo;
					
					leerXml = leerXml+"\n"+"El Equip "+ element.getElementsByTagName("nombre").item(0).getTextContent()+"  Victorias: "+element.getElementsByTagName("victoria").item(0).getTextContent();

					//System.out.println("Equipo " + element.getElementsByTagName("nombre").item(0).getTextContent());
					//System.out.println("Tipo: " + element.getElementsByTagName("tipo").item(0).getTextContent());
					//System.out.println("Vicoria: " + element.getElementsByTagName("victoria").item(0).getTextContent());

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}
	void leerXML(ArrayList <Planetas> ganador) {
		leerXml = "Lectura XML";

		// TODO Auto-generated method stub
		ArrayList<Planetas> ranking = new ArrayList<Planetas>(); // Equips que crearem
		try {
			File archivo = new File("E:\\Dam2 2020\\coldward-cuni\\ranking.xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document document = documentBuilder.parse(archivo);
			document.getDocumentElement().normalize();
			System.out.println("Elemento raiz:" + document.getDocumentElement().getNodeName());
			NodeList listaEmpleados = document.getElementsByTagName("equipos");
			for (int temp = 0; temp < listaEmpleados.getLength(); temp++) {
				Planetas planetas = new Planetas("");

				Node nodo = listaEmpleados.item(temp);
				System.out.println("Elemento:" + nodo.getNodeName());
				if (nodo.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) nodo;
					
					planetas.setEquip(element.getElementsByTagName("nombre").item(0).getTextContent());
					planetas.setVictoria(Integer.parseInt(element.getElementsByTagName("victoria").item(0).getTextContent()));
					ranking.add(planetas);
					//System.out.println("Equipo " + element.getElementsByTagName("nombre").item(0).getTextContent());
					//System.out.println("Tipo: " + element.getElementsByTagName("tipo").item(0).getTextContent());
					//System.out.println("Vicoria: " + element.getElementsByTagName("victoria").item(0).getTextContent());
					leerXml = leerXml+"\n"+"El Equip "+ element.getElementsByTagName("nombre").item(0).getTextContent()+"  Victorias: "+element.getElementsByTagName("victoria").item(0).getTextContent();
				}
				leerXml = "Lectura XML";
			}
			int cont = 0;
			
			for(int y = 0; y<ranking.size(); y++) {				
				if(ranking.get(y).getEquip().equals(ganador.get(0).getEquip())) {
					int numvictorias = ranking.get(y).getVictoria() + 1;
					ranking.get(y).setVictoria(numvictorias);
					cont = 1;
				}	
			}
			if(cont==0) {
				ranking.add(ganador.get(0));
			}
			
			crearXML(ranking);
			
		} catch (Exception e) {
			e.printStackTrace();
			ranking.add(ganador.get(0));
			crearXML(ranking);
		}
	}
	  void crearXML(ArrayList <Planetas> ranking){	 

		try {
			
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// elemento raiz

			
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("ranking");

			doc.appendChild(rootElement);

			for(int x = 0; x< ranking.size();x ++) {

			// equipo
			Element equipos = doc.createElement("equipos");
			rootElement.appendChild(equipos); 

			// Nombre
			Element nombre = doc.createElement("nombre");
			nombre.appendChild(doc.createTextNode(ranking.get(x).getEquip()));
			equipos.appendChild(nombre);
			// Victoria

			Element victories = doc.createElement("victoria");

			victories.appendChild(doc.createTextNode(String.valueOf(ranking.get(x).getVictoria())));

			equipos.appendChild(victories);
		}
			// escribimos el contenido en un archivo .xml

			TransformerFactory transformerFactory = TransformerFactory.newInstance();

			Transformer transformer = transformerFactory.newTransformer();

			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File("E:\\Dam2 2020\\coldward-cuni\\ranking.xml"));
			transformer.transform(source, result);

			System.out.println("File saved!");

		} catch (ParserConfigurationException pce) {

			pce.printStackTrace();

		} catch (TransformerException tfe) {

			tfe.printStackTrace();

		}

	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		Object botonPulsado = e.getSource();

		if (botonPulsado == jugar) {

			this.removeAll();
			this.repaint();
			Main.contenido = 1;
			ContingutsDiferents();
		}
		else if(botonPulsado == informacio) {
			//Anar a informacio
			this.removeAll();
			this.repaint();
			Main.contenido = 3;	
			ContingutsDiferents();
		}

		else if(botonPulsado == sortir) {
			//Sortir del joc
			System.exit(0);
		}
		else if(botonPulsado == tornar) { 
			//Tornar al inici
			this.removeAll();
			this.repaint();
			equips.clear();
			Main.contenido = 0;
			ContingutsDiferents();
		}
		else if(botonPulsado == crearEquip) {
			
			//Crear equips
			
			if(Integer.parseInt(numeroEquipsT.getText()) != 3) {
				JOptionPane.showMessageDialog(null, "Nomes es poden jugar un minim de 3 jugadors");
			}else {
				this.removeAll();
				this.repaint();
			
			System.out.println("Numero Equips "+Integer.parseInt(numeroEquipsT.getText()));
			Main.contenido = 2;
			ContingutsDiferents();
			}
		}

		else if(botonPulsado == equipNou) {
			//Crear equips
			this.removeAll();
			this.repaint();



			if(con < Integer.parseInt(numeroEquipsT.getText())) {
				con++;

				//Guarda el nom del planeta al arraylist
				System.out.println("Nom del equip "+nomEquip.getText());
				Planetas p = new Planetas(nomEquip.getText());
				p.setTipo(tipoPlaneta);
				
				if(tipoPlaneta == 1) {
					p.setTipo(1);
				}else if (tipoPlaneta == 2) {
					p.setVida(400);
					p.setMisils(100);
					p.setTipo(2);
				}else if (tipoPlaneta == 3) {
					p.setMisils(100);
					p.setTipo(3);

				}else if (tipoPlaneta == 4) {
					p.setMisils(100);
					p.setVida(400);
					p.setTipo(4);

				}else if (tipoPlaneta == 5) {
					p.setVida(400);
					p.setTipo(5);
				}
				equips.add(p);
				

				if(con < Integer.parseInt(numeroEquipsT.getText())) {
					Main.contenido = 2;
				}else {
					Main.contenido = 4;
				}
				ContingutsDiferents();
			}else {
				this.removeAll();
				this.repaint();
				Main.contenido = 4;
				ContingutsDiferents();
			}
			//control +2 +r 
		}
		else if(botonPulsado == atac) {
			this.removeAll();
			this.repaint();

			int nummisils;

			nummisils = comboMisils.getSelectedIndex()+1;
			equipAtacat = (String) comboNombreEquipos.getSelectedItem();

			for(int x = 0;x<equips.size();x++) {
				Planetas planetas = new Planetas("");
				planetas = equips.get(x);

				System.out.println(planetas.getVida());
				if(planetas.getEquip().equals(equipAtacat)) {
					double mis = planetas.getMisils() - nummisils;
					planetas.setMisils(mis);
					double vidaResta;
					vidaResta =planetas.getVida()-nummisils; 
					planetas.setVida(vidaResta);

				}
			}
			misils = misils - nummisils;
			textAfegir =textAfegir+" " +"El equip "+equipEnPartida+ " a atacat " + equipAtacat+ " amb " + nummisils+ " misils \n " ;


			if(misils==0) {
				numTurno ++;
				misils = 50;
				if (numTurno==equips.size()+1) {

					numTurno=1;
					numRonda ++;
					equips.get(0).setRonda(numRonda);

					for (int x=0; x<equips.size();x++) {	
						vidaEquips = vidaEquips +" "+ equips.get(x).getEquip()+" la seva vida es " +equips.get(x).getVida()+"\n ";
					}

					Main.contenido = 6;
					ContingutsDiferents();					
				}else {
					Main.contenido = 4;
					ContingutsDiferents();
				}
			}else {
				Main.contenido = 4;
				ContingutsDiferents();
			}


		}	
		else if(botonPulsado == defensar) {
			this.removeAll();
			this.repaint();
			int numdef = comboMisils.getSelectedIndex()+1;
			int numdef2 = numdef/2;

			equipDefensat = equipEnPartida;

			textAfegir = textAfegir + "El equip "+equipDefensat+ " s'ha defensat amb " + numdef2+ " de vida \n ";
			for (int x=0; x<equips.size();x++) {
				if(equips.get(x).getEquip().equals(equipEnPartida)) {
					double vidaTotal = equips.get(x).getVida() + numdef2;
					equips.get(x).setVida(vidaTotal);  ;
				}
			}
			misils = misils - numdef;

			if(misils==0) {
				numTurno ++;
				misils = 50;
				if (numTurno==equips.size()+1) {
					numTurno=1;
					numRonda ++;
					equips.get(0).setRonda(numRonda);
					for (int x=0; x<equips.size();x++) {
						if(equips.get(x).getVida()<= 0) {
							equips.remove(equips.get(x));
						}
					}
					for (int x=0; x<equips.size();x++) {
						vidaEquips = vidaEquips +" "+ equips.get(x).getEquip()+" la seva vida es " +equips.get(x).getVida()+"\n ";
					}

					Main.contenido = 6;
					ContingutsDiferents();					
				}else {
					Main.contenido = 4;
					ContingutsDiferents();
				}

			}else {
				Main.contenido = 4;
				ContingutsDiferents();
			}
		}else if (botonPulsado == siguiente) {
			this.removeAll();
			this.repaint();
			textAfegir = " ";
			vidaEquips = " ";
			Main.contenido = 4;
			ContingutsDiferents();
		}else if (botonPulsado == ranking) {
			this.removeAll();
			this.repaint();
			
			Main.contenido = 7;
			ContingutsDiferents();
		}else if (botonPulsado == guarda) {
		this.removeAll();
		this.repaint();
		insertWithStatement(connection, equips);
		Main.contenido = 7;
		ContingutsDiferents();
		}else if (botonPulsado == cargar_Partida) {
			this.removeAll();
			this.repaint();
			selectWithStatement(connection);
			Main.contenido = 8;
			ContingutsDiferents();
		}else if (botonPulsado == partidaG) {
			this.removeAll();
			this.repaint();
			carregar_Partida = partidas.getSelectedIndex()+1;
			carregarPartida(connection);
			Main.contenido = 4;
			ContingutsDiferents();
		}
	}
	
	
public static Connection makeConnection() {
		
		System.out.println("Connecting database...");
		
		Connection cone = null;
		//intentamos la conexion a la base de datos
		try  {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			cone = DriverManager.getConnection(URL);
			System.out.println("Database connected!");
			
		} catch (SQLException e) {
		    throw new IllegalStateException("Cannot connect the database! ", e);
		    
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		//devolvemos el valor de la conexion
		return cone;		
	}

	
	public static void closeConnection(Connection cone) {
		//cierra la conexi�n
		try {
			cone.close();
			
		} catch (SQLException e) {
			System.out.println("Error closing connection!!" + e);
			
		}
	}
	
	
	public static void insertWithStatement(Connection con, ArrayList <Planetas> p) {
		String sql = "SELECT id_partida FROM dbo.colward_cuni;";
		Statement st = null;
		int id_partida = 1;
		try {
			st = con.createStatement();
			
		    ResultSet rs = st.executeQuery(sql);
		       
		    while (rs.next())
		    {
		    	 id_partida = rs.getInt("id_partida");
		    	 id_partida ++;
		        System.out.println("Id:" + id_partida );
		        
		    }
		      
		    st.close();
		    
		} catch (SQLException e) {
			System.out.println("The SELECT had problems!! " + e);
			
		}
		
		for(int x = 0; x< p.size(); x ++) {
			String equip = p.get(x).getEquip();
			Double misils = p.get(x).getMisils();
			int tipo = p.get(x).getTipo();
			int victoria = p.get(x).getVictoria();
			Double vida = p.get(x).getVida();
			int ronda = p.get(x).getRonda();
			String sql1 = "INSERT INTO dbo.colward_cuni (data,id_partida,misils,vida,equip,tipo,victoria,ronda ) VALUES (GETDATE(),'"+id_partida+"', '"+misils+"', '"+vida+"', '"+equip+"', '"+tipo+"', '"+victoria+"', '"+ronda+"');";
			try {
			Statement statement = (Statement) con.createStatement();
			statement.execute(sql1);
			statement.close();
			
			} catch (SQLException e) {
			System.out.println("The Insert had problems!! " + e);
			
		} 
		}
	}
	
	public static void selectWithStatement(Connection con) {
		String sql = "SELECT id_partida FROM dbo.colward_cuni;";
		Statement st = null;
		id_partidac = 0;
		try {
			st = con.createStatement();
			
		    ResultSet rs = st.executeQuery(sql);
		       
		    while (rs.next())
		    {
		    	id_partidac = rs.getInt("id_partida");
		        System.out.println("Id:" + id_partidac );
		        
		    }
		      
		    st.close();
		    
		} catch (SQLException e) {
			System.out.println("The SELECT had problems!! " + e);
			
		}
	}
	public static void carregarPartida(Connection con) {
		ArrayList<Planetas> partida_guardada = new ArrayList<Planetas>(); // Equips que crearem
		int  numero_equip = 0;
		String sql = "SELECT * FROM dbo.colward_cuni where id_partida = '"+carregar_Partida+"';";
		Statement st = null;
		id_partidac = 0;
		try {
			st = con.createStatement();
			
		    ResultSet rs = st.executeQuery(sql);
		       
		    while (rs.next())
		    {
		    	Planetas p = new Planetas(" ");
		    	p.setEquip(rs.getString("equip"));
		    	p.setVida(rs.getDouble("vida"));
		    	p.setRonda(rs.getInt("ronda"));
		    	p.setVictoria(rs.getInt("victoria"));
		    	p.setTipo(rs.getInt("tipo"));
		    	p.setMisils(50);
		    	//System.out.println("Id:" + id_partidac );
		        partida_guardada.add(p);
		    }
		    equips = partida_guardada;
		    st.close();
		    
		} catch (SQLException e) {
			System.out.println("The SELECT had problems!! " + e);
			
		}
	}
}